public class SincornizacionMain {

    public static final int NUMERO_DE_WORKERS = 10;

    public static void main(String[] args) {

        ContadorWorker[] contadorWorkers = new ContadorWorker[NUMERO_DE_WORKERS];
        Thread[] contadorThreads = new Thread[NUMERO_DE_WORKERS];

        AcumuladorContador ac = new AcumuladorContador();

        //Primero los creamos
        for (int i = 0; i<contadorWorkers.length;i++){
            contadorWorkers[i] = new ContadorWorker(ac);
            contadorThreads[i] = new Thread(contadorWorkers[i]);
        }

        //Ejecutamos los hilos
        for (Thread t: contadorThreads) {

            t.start();

        }

        for (Thread t: contadorThreads){
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(ac.getAcumulador());

    }

}
