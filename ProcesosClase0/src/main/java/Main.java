import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ProcessBuilder pb = new ProcessBuilder("ls");


        try {
            Process ls = pb.start();
            System.out.println("PADRE: El resultado del" +
                    " hijo es: " + ls.waitFor());

            Scanner salidaHijo = new Scanner(ls.getInputStream());
            String linea = salidaHijo.nextLine();

            System.out.println(linea
            );

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
