public class ContadorWorker implements Runnable{

    AcumuladorContador ac;

    public ContadorWorker(AcumuladorContador acumuladorContador){
        this.ac = acumuladorContador;
    }

    @Override
    public void run() {

        for (int i = 0; i< 10; i++){

            System.out.println(
                    Thread.currentThread().getName() + " => " + ac.getAcumulador());

            ac.incrementarAcumulador();

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }



        }

    }
}
