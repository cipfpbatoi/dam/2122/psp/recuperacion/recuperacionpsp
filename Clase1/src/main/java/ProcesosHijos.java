import java.io.*;
import java.util.Scanner;

public class ProcesosHijos {

    
    public static void main(String[] args) {

        ProcessBuilder pb =
                new ProcessBuilder("java","-jar","Hijo1.jar");


        try {
            //Arrancar proceso hijo
            Process hijo1 = pb.start();

            //Leer lo que escribe proceso hijo en su salida estándar
            Scanner scHijo = new Scanner(hijo1.getInputStream());

            //Leer la entrada de error
            Scanner scErrorHijo = new Scanner(hijo1.getErrorStream());

            //Escribir en la entrada estándar del proceso hijo
            PrintWriter pw = new PrintWriter(hijo1.getOutputStream(),
                    true);

            //Leer desde la entrada estándar
            Scanner sc = new Scanner(System.in);

            //Empezamos el Ritual:

            String lineaUsuario;

            while ( !(lineaUsuario = sc.nextLine()).equalsIgnoreCase("stop")){
                pw.println(lineaUsuario);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
