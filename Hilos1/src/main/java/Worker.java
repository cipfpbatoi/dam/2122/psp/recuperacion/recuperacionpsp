public class Worker implements Runnable {

    void metodoQueImprimeElNombreDelHilo(){
        System.out.println("Metodo de Worker: "+ Thread.currentThread().getName());
    }

    @Override
    public void run(){
        metodoQueImprimeElNombreDelHilo();
    }

}
